<?php

/**
 * @file
 * Theme settings form for cascade theme.
 */

use Drupal\Core\Entity\ContentEntityType;

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function cascade_form_system_theme_settings_alter(&$form, &$form_state) {
  $options = [];
  /* @var $definition EntityTypeInterface */
  foreach (\Drupal::entityTypeManager()->getDefinitions() as $definition) {
    if ($definition instanceof ContentEntityType) {
      $options[$definition->id()] = $definition->getLabel();
    }
  }

  $form['cascade']['features'] = [
    '#type' => 'details',
    '#title' => t('Features'),
    '#open' => TRUE,
    'anchor_links' => [
      '#type' => 'checkbox',
      '#title' => t('Anchor Links'),
      '#default_value' => theme_get_setting('anchor_links'),
      '#description' => t("Preprocess links and entity-ids for improved navigation with lots of entities on one page")
    ]
  ];

  $form['cascade']['types'] = [
    '#type' => 'details',
    '#title' => t('Generic Types'),
    '#open' => TRUE,
    'types' => [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => theme_get_setting('types'),
      '#description' => t("For each selection, generic CSS will be injected to each view display of the type")
    ]
  ];
}
