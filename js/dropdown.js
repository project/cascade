/**
 * @file
 * Dropdown behaviors.
 */

(function (Drupal, Popper, once) {
	'use strict';

	Drupal.behaviors.dropdown_behavior = {
		attach(context) {
			// @see https://www.drupal.org/project/once/issues/3204168#comment-14033931
			const dds = once('dropdown_behavior', context.querySelectorAll('.dropdown-wrapper'));
			if (dds.length) {
				dds.forEach(dd => {
					Popper.createPopper(dd.querySelector('a, button'), dd.querySelector('.dropdown-content'), {
						placement: 'auto',
						modifiers: [
							{
								name: 'flip',
								options: {
									allowedAutoPlacements: ['top', 'bottom'],
								},
							}
						],
					});
				});
			}
		}
	};
}(Drupal, Popper, once));